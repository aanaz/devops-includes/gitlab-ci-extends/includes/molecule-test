# Docker build

This job configuration allows you to test Ansible-roles.

### How to include
```yml
stages:
  - test

include:
  - project: 'aanaz/devops-includes/gitlab-ci-extends/includes/molecule-test'
    ref: main
    file: molecule-test.yml

molecule-test:
  extends: .molecule-test
```
